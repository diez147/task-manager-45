package ru.tsc.babeshko.tm.api.service.model;

import ru.tsc.babeshko.tm.api.repository.model.IRepository;
import ru.tsc.babeshko.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}