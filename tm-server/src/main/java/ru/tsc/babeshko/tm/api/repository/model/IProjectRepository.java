package ru.tsc.babeshko.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    void removeAllByUserId(@Nullable String userId);

}
