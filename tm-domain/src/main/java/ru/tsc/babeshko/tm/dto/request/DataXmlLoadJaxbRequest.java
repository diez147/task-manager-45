package ru.tsc.babeshko.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlLoadJaxbRequest extends AbstractUserRequest {

    public DataXmlLoadJaxbRequest(@Nullable String token) {
        super(token);
    }

}