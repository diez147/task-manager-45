package ru.tsc.babeshko.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    public ProjectUpdateByIndexResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}